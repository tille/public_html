#!/usr/bin/python3

## TODO
## Sponsored
## select distinct source from upload_history where signed_by_email = 'tille@debian.org' and not changed_by_email = 'tille@debian.org' order by source;
##  --> `not changed` is too simple - better not in uploaders in sources
## WNPP

from sys import stderr, exit
import psycopg2
import re

NAME='Andreas Tille'
EMAIL='tille@debian.org'
publishfile='index.html'

# PORT = 5441
UDDPORT = 5452
PORT = UDDPORT
DEFAULTPORT = 5432

###############################################################################
connection_pars = [
#    {'service': 'udd'},
#    {'host': 'localhost', 'port': PORT, 'user': 'guest', 'database': 'udd'},         ##FIXME: use this for development, than deactivate
#    {'host': 'localhost', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},  ##FIXME: use this for development, than deactivate

    # Public UDD mirror for real queries
    {'host': 'udd-mirror.debian.net', 'port': 5432,
     'user': 'udd-mirror', 'password': 'udd-mirror', 'database': 'udd'},
]
conn = None
for par in connection_pars:
    try:
        conn = psycopg2.connect(**par)
        break
    except psycopg2.OperationalError as err:
        continue

if conn is None:
    stderr.write("Connection to UDD can't be established by any of\n  %s" % str(connection_pars))
    exit(-1)

conn.set_client_encoding('utf-8')
curs = conn.cursor()
# uddlog = open('logs/uddquery.log', 'w')

def RowDictionaries(cursor):
    """Return a list of dictionaries which specify the values by their column names"""

    if not cursor.description:
        # even if there are no data sets to return the description
        # should contain the table structure.  If not something went
        # wrong and we return None as to represent a problem
        return None

    try:
        return [dict((dd[0], dv) for (dd, dv) in zip(cursor.description, row))
                for row in cursor]
    except NameError:
        return [dict((dd[0], dv) for (dd, dv) in zip(cursor.description, row))
                for row in cursor]

def _execute_udd_query(query, arg):
    try:
        curs.execute(query, arg)
    except psycopg2.ProgrammingError as err:
        stderr.write("Problem with query\n%s\n%s\n" % (query, str(err)))
        exit(-1)
    except psycopg2.DataError as err:
        stderr.write("%s; query was\n%s\n" % (str(err), query))

query = """SELECT year, count(*) AS uploads FROM
     (SELECT EXTRACT(year FROM date)::int AS year, CASE WHEN changed_by_name = '' THEN maintainer_name ELSE changed_by_name END AS name FROM upload_history) uh
     WHERE name ilike %s GROUP BY year, name ORDER by year;
"""

_execute_udd_query(query, ('%' + NAME + '%',))

my_uploads = RowDictionaries(curs)

query = """SELECT count(*) AS uploads, name FROM 
  (SELECT CASE WHEN changed_by_name = '' THEN maintainer_name ELSE changed_by_name END AS name FROM upload_history) uh 
  GROUP BY name ORDER by uploads DESC limit 5;
"""

_execute_udd_query(query, ())

num_uploads=0
for row in RowDictionaries(curs):
    if row['name'] == NAME:
       print("I'm the Debian developer with the most uploads (%i currently)." % row['uploads'])
       num_uploads=row['uploads']
if num_uploads == 0:
    print("Failed to detect number of uploads")
    exit(1)


query = "SELECT COUNT(*) FROM (SELECT DISTINCT source FROM sources WHERE maintainer_email = %s OR uploaders LIKE %s) tmp"

_execute_udd_query(query, (EMAIL, '%' + EMAIL + '%',))

num_responsible=curs.fetchone()
if num_responsible == 0:
    print("Failed to detect number of uploads")
    exit(1)
print("I'm resonsible for %i packages (as Maintainer or Uploader)." % num_responsible)

query = """SELECT year, count(*) AS bugs FROM (
      SELECT year, CASE WHEN name = 'Tille, Andreas' THEN 'Andreas Tille' ELSE name END AS name FROM (
        SELECT EXTRACT(year FROM last_modified)::int AS year,
               CASE WHEN ab.done_name = '' THEN cn.name ELSE BTRIM(done_name, '"') END AS name,
               ab.done,
               ab.done_name,
               ab.done_email
          FROM (SELECT id, last_modified, done, done_name, done_email FROM archived_bugs
                UNION
                SELECT id, last_modified, done, done_name, done_email FROM bugs WHERE status = 'done'
          ) ab
          LEFT JOIN carnivore_emails ce ON ce.email = ab.done_email
          LEFT JOIN carnivore_names  cn ON ce.id    = cn.id
       ) ab
     ) zw
     WHERE name ilike %s GROUP BY year, name ORDER by year
"""

_execute_udd_query(query, ('%' + NAME + '%',))

my_bugs = RowDictionaries(curs)

query="""SELECT ROW_NUMBER() OVER () AS nr, count AS bugs, name FROM (
 SELECT count(*) as count, name FROM (
  SELECT CASE WHEN name is null OR name = '' THEN
        regexp_replace(regexp_replace(regexp_replace(regexp_replace(done, '[0-9]+@bugs.debian.org, *', ''), '( \([^(]+) \(.*\)\)', '\1)'), '.* \(([^)]+)\)', '\1'), ',? <.*', '')
        ELSE name END AS name FROM (
    SELECT CASE WHEN ab.done_name = '' THEN CASE WHEN cn.name = '' THEN LOWER(ab.done_email) ELSE cn.name END ELSE BTRIM(done_name, '"') END AS name,
               ab.done,
               ab.done_name,
               LOWER(ab.done_email) AS done_email
          FROM (SELECT id, done, done_name, done_email FROM archived_bugs
                UNION
                SELECT id, done, done_name, done_email FROM bugs WHERE status = 'done') ab
          LEFT JOIN carnivore_emails ce ON LOWER(ce.email) = LOWER(ab.done_email)
          LEFT JOIN (
             SELECT id, name FROM (
                SELECT id, name, ROW_NUMBER() OVER (PARTITION BY id ORDER BY LENGTH(name) DESC) AS rn FROM carnivore_names) AS ranked
                WHERE rn = 1
             ) cn ON ce.id    = cn.id
     WHERE LOWER(ab.done_email) not in ('ftpmaster@ftp-master.debian.org','noreply@salsa.debian.org','unknown')
   ) rname
  ) ab GROUP BY name ORDER by count DESC
) x
limit 20;
"""

_execute_udd_query(query, ())

list_bugs=0
num_bugs=0
for row in RowDictionaries(curs):
    if row['name'] == NAME:
        print("I'm number %(nr)i in the list of people who have fixed the most bugs in Debian and I have currently fixed %(bugs)i bugs." % row)
        list_bugs=row['nr']
        num_bugs=row['bugs']
if list_bugs == 0 or num_bugs == 0:
    print("Failed to detect number of bugs")
    exit(1)

my_stats={}
for my in my_uploads:
    my_stats[my['year']]={}
    my_stats[my['year']]['uploads'] = my['uploads']
for my in my_bugs:
    if my['year'] not in my_stats:
        my_stats[my['year']]={}
    my_stats[my['year']]['bugs'] = my['bugs']

htout="""
<table>
<tr><th>Year</th><th>Uploads</th><th>Bugs fixed</th></tr>
"""
for row in my_stats:
    #print(row, my_stats[row])
    if 'bugs' not in my_stats[row]:
        my_stats[row]['bugs'] = 0
    htout += "<tr><td>%i</td><td align=right>%i</td><td align=right>%i</td></tr>\n" % (row, my_stats[row]['uploads'], my_stats[row]['bugs'])
htout += "</table>"

with open(publishfile, 'r') as file:
    data = file.read()

pattern = re.compile(r'<!-- Beginn statistics -->.*?<!-- End statistics -->', re.DOTALL)
nu_pattern = re.compile(r'<!--numuploads_begin-->.*<!--numuploads_end-->', re.DOTALL)
lb_pattern = re.compile(r'<!--lbugs_begin-->.*<!--lbugs_end-->', re.DOTALL)
nb_pattern = re.compile(r'<!--numbugs_begin-->.*<!--numbugs_end-->', re.DOTALL)

new_text = '<!-- Beginn statistics -->\n' + htout + '<!-- End statistics -->'
data_new = re.sub(pattern, new_text, data)
data_new = re.sub(nu_pattern, '<!--numuploads_begin-->%i<!--numuploads_end-->' % num_uploads, data_new)
data_new = re.sub(lb_pattern, '<!--lbugs_begin-->%i<!--lbugs_end-->' % list_bugs, data_new)
data_new = re.sub(nb_pattern, '<!--numbugs_begin-->%i<!--numbugs_end-->' % num_bugs, data_new)

with open(publishfile, 'w') as file:
    file.write(data_new)

#print(htout)

## debug: SELECT COUNT(*) FROM archived_bugs WHERE done_name ilike '%Andreas%Tille%' ;


# wnpp

# packages sponsored

